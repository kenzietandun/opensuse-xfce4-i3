# XFCE4 + i3WM for openSUSE

This is a install script for part V of [this guide](https://feeblenerd.blogspot.com/2015/11/pretty-i3-with-xfce.html) for openSUSE

Tested for openSUSE Tumbleweed

### Overall Setup Steps


##### Install XFCE with openSUSE

Choose the XFCE desktop when installing openSUSE

##### Install dependencies to make i3wm work with XFCE

This is when you run this script, `bash install.sh`. This is will ask for root password at some point as it requires installing using zypper and some `make` commands.

##### Disable XFCE's xfwm and xfdesktop

Follow the steps from part VI of the blog linked above

##### Add i3 to startup

Follow the steps from part VII of the blog linked above

##### Disable default keyboard shortcuts

Follow the steps from part VIII of the blog linked above

##### Re-login

Enjoy!
