#!/bin/bash 

mkdir -p ~/git
cd ~/git/

# Require i3ipc-glib
git clone https://github.com/altdesktop/i3ipc-glib

# Install dependencies for i3ipc-glib
sudo zypper in -y gtk-doc autoconf xcb-proto-devel gobject-introspection gobject-introspection json-glib-devel automake libtool gcc xcb

cd i3ipc-glib/

./autogen.sh --prefix=/usr && make && sudo make install

sudo zypper in -y nitrogen i3-gaps

cd ~/git/

# Require xfce4-workspaces-plugin
git clone https://github.com/denesb/xfce4-i3-workspaces-plugin.git

cd xfce4-i3-workspaces-plugin/

sudo zypper in -y libxfce4ui-devel libxfce4util-devel libxfce4ui-tools xfce4-dev-tools xfce4-panel-devel
PKG_CONFIG_PATH=/usr/lib/pkgconfig ./autogen.sh --prefix=/usr && make && sudo make install
